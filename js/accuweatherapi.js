const MAIN = "http://dataservice.accuweather.com",
    LINK_CURRENT_TEMPERATURE = "/currentconditions/v1",
    LINK_HOURLY = "/forecasts/v1/hourly/12hour",
    LINK_DAILY = "/forecasts/v1/daily/1day",
    LINK_CITY_CODE = "/296543",
    API_KEY = "LqH0AKwi2FrPPAPAlzcuEelQMVgYuwrv",
    LINK_PROPERTIES = "unit=c&language=ru-ru";
const FORECAST_TYPE_DAILY = 0,
    FORECAST_TYPE_HOURLY = 1,
    FORECAST_TYPE_CURRENT = 2;

class AccuWeatherAPI {

    static getCurrentTemperature(textElementId, imageElementId) {
        xhr = new XMLHttpRequest();
        xhr.open("GET", AccuWeatherAPI.generate_url(LINK_CURRENT_TEMPERATURE), true);
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                if (xhr.status == 200) {
                    json = JSON.parse(xhr.responseText);
                    document.getElementById(textElementId).textContent = parseInt(json.main.temp) + "\u2103";
                    document.getElementById(imageElementId).src = "./images/weather_icons/" + json.weather[0].icon + ".png";
                }
            }
        };
        xhr.send();
    }

    static getForecastTemperature(forecastType) {
        xhr = new XMLHttpRequest();
        xhr.open("GET", AccuWeatherAPI.generate_url(forecastType == FORECAST_TYPE_DAILY ? LINK_DAILY : LINK_HOURLY), true);
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                if (xhr.status == 200) {
                    processBars(JSON.parse(xhr.responseText));
                }
            }
        };
        xhr.send();
    }

    static parseResponse(forecastType, json) {

    }

    static parseCurTemp(json) {

    }

    static parseHourlyForecast(json) {
        inputData = [];
        for (i = 0; i < json.list.length; i++) {
            inputData[i] = json.list[i].main.temp;
        }
        columnNames = [];
        switch (barsType) {
            case BARS_TYPE_HOURLY_FORECAST: {
                columnNames = getHoursList(json.list[0].dt_txt, json.list[6].dt_txt);
                break;
            }
            case BARS_TYPE_DAILY_FORECAST: {
                columnNames = getDayListStartingFrom(new Date().getDay() - 1);
                break;
            }
        }
    }

    static parseDailyForecast(json) {

    }

    static generate_url(forecastType) {
        switch (forecastType) {
            case FORECAST_TYPE_CURRENT: {
                return AccuWeatherAPI.gen_url(LINK_CURRENT_TEMPERATURE);
            }
            case FORECAST_TYPE_HOURLY: {
                return AccuWeatherAPI.gen_url(LINK_HOURLY);
            }
            case FORECAST_TYPE_DAILY: {
                return AccuWeatherAPI.gen_url(LINK_DAILY);
            }
        }
    }

    static gen_url(link) {
        return MAIN +
            link +
            LINK_CITY_CODE +
            "?" + LINK_PROPERTIES +
            "&apikey=" + API_KEY;
    }
}