/*
 * Cirulcar Calendar Display.js
 * Matthew Juggins
 * Change log:
 * 		25/09/16 - Quick fix to day of the week
 */
const BARS_TYPE_DAILY_FORECAST = 0,
    BARS_TYPE_HOURLY_FORECAST = 1;
var MAIN = "http://api.openweathermap.org/data/2.5",
    LINK_DAY_WEATHER = "/weather?",
    LINK_WEATHER_FORECAST = "/forecast?",
    LINK_HOURLY_FORECAST = "/forecast/hourly?",
    CITY_ID = "472045",
    API_KEY = "553114e074188be78d68314c496c79f6",
    WEATHER_UNITS = "Metric";
var dayNames = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
var barsType = BARS_TYPE_HOURLY_FORECAST;

class OpenWeatherMapAPI {
    generate_url(forecastType) {
        switch (forecastType) {
            case 1: {
                return OpenWeatherMapAPI.gen_url("/weather?");
            }
            case 2: {
                return OpenWeatherMapAPI.gen_url("/forecast?");
            }
            case 3: {
                return OpenWeatherMapAPI.gen_url("/forecast/hourly?");
            }
        }
    }

    static gen_url(link) {
        return "http://api.openweathermap.org/data/2.5" +
            link +
            "id=472045" +
            "&appid=553114e074188be78d68314c496c79f6" +
            "&units=Metric";
    }
}

class AccuWeatherAPI {
    generate_url(forecastType) {
        alert("generate url type " + forecastType);
        switch (forecastType) {
            case 1: {
                return AccuWeatherAPI.gen_url("/currentconditions/v1");
            }
            case 2: {
                return AccuWeatherAPI.gen_url("/forecasts/v1/hourly/12hour");
            }
            case 3: {
                return AccuWeatherAPI.gen_url("/forecasts/v1/daily/1day");
            }
        }
    }

    static gen_url(link) {
        return "http://dataservice.accuweather.com" +
            link +
            "/296543" +
            "?unit=c&language=ru-ru" +
            "&apikey=LqH0AKwi2FrPPAPAlzcuEelQMVgYuwrv";
    }

    parseHourlyForecast(json, callbackFunction) {
        let inputData = [];
        for (i = 0; i < json.list.length; i++) {
            inputData[i] = json.list[i].main.temp;
        }
        let columnNames = [];
        switch (barsType) {
            case BARS_TYPE_HOURLY_FORECAST: {
                columnNames = getHoursList(json.list[0].dt_txt, json.list[6].dt_txt);
                break;
            }
            case BARS_TYPE_DAILY_FORECAST: {
                columnNames = getDayListStartingFrom(new Date().getDay() - 1);
                break;
            }
        }
        callbackFunction(inputData, columnNames);
    }
}

var api = new AccuWeatherAPI();
var parseXml;
var horoXML;
var quoteObject;
var curHoro = 0;

function generate_url(link) {
    return MAIN +
        link +
        "id=" + CITY_ID +
        "&appid=" + API_KEY +
        "&units=" + WEATHER_UNITS;
}

function checkTemp() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", generate_url(LINK_DAY_WEATHER), true);
    xhr.onload = function () {
        if (xhr.readyState === 4) {
            if (xhr.status == 200) {
                processTemp(JSON.parse(xhr.responseText));
            }
        }
    };
    xhr.send();
}

function switchBars() {
    if (barsType == BARS_TYPE_HOURLY_FORECAST) {
        barsType = BARS_TYPE_DAILY_FORECAST;
    } else {
        barsType = BARS_TYPE_HOURLY_FORECAST;
    }
    loadBars();
}

function loadHoro() {
    let horoElement = document.getElementById("horo"),
        xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.readyState === 4) {
            if (xhr.status == 200) {
                horoElement.style.visibility = "visible";
                horoXML = xhr.responseXML;
                setHoro(getNextValidNode(horoXML.getElementsByTagName("horo")[0].getElementsByTagName("taurus")[0]));
            } else {
                horoElement.style.visibility = "hidden";
            }
        }
    };
    xhr.onerror = function (e) {
        horoElement.style.visibility = "hidden";
    };
    xhr.open("GET", "http://img.ignio.com/r/export/utf/xml/daily/com.xml");
    xhr.responseType = "document";
    xhr.send();
}

function switchHoro() {
    setHoro(getNextValidNode(horoXML.getElementsByTagName("horo")[0].getElementsByTagName("taurus")[0]));
}

function setHoro(horoNode) {
    let dayName = horoNode.tagName.charAt(0).toUpperCase() + horoNode.tagName.slice(1);
    if (dayName == "Tomorrow02") dayName = "Tomorrow+";
    document.getElementById("horo_day").textContent = dayName;
    document.getElementById("horo_message").textContent = horoNode.textContent;
}

function getNextValidNode(nodeList) {
    let startIndex = curHoro;
    if (curHoro + 1 >= nodeList.childNodes.length - 1) {
        startIndex = -1;
    }
    for (let i = startIndex + 1; i < nodeList.childNodes.length; i++) {
        if (nodeList.childNodes[i].nodeType == 1) {
            curHoro = i;
            return nodeList.childNodes[i];
        }
    }
}

function loadQuote() {
    let quoteElement = document.getElementById("quote"),
        quoteText = document.getElementById("quote_text"),
        quoteAuthor = document.getElementById("quote_author"),
        xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                quoteObject = parseXml(xhr.responseText).getElementsByTagName("forismatic")[0].getElementsByTagName("quote")[0];
                quoteText.textContent = quoteObject.getElementsByTagName("quoteText")[0].textContent;
                quoteAuthor.textContent = quoteObject.getElementsByTagName("quoteAuthor")[0].textContent;
                if (localStorage.getItem('favoriteQuotes').contains(getQuoteId())) {
                    document.getElementById("quote_add_to_favorite").style.background = "url('./images/icons/star_checked.png')";
                } else {
                    document.getElementById("quote_add_to_favorite").style.background = "url('./images/icons/star_unchecked.png')";
                }
                quoteElement.style.visibility = "visible";
            } else {
                quoteElement.style.visibility = "hidden";
            }
        }
    };
    xhr.onerror = function (e) {
        quoteElement.style.visibility = "hidden";
    };
    xhr.open("GET", "http://api.forismatic.com/api/1.0/getQuote?method=getQuote&key=457653&format=xml&lang=ru");
    xhr.send();
}

function getQuoteId() {
    let link, id;
    link = quoteObject.getElementsByTagName("quoteLink")[0].textContent;
    id = link.replace("http://forismatic.com/ru/", "");
    id = id.replace("/", "");
    return id;
}

function addQuoteToFavorites() {
    let id = getQuoteId();
    if (localStorage.getItem('favoriteQuotes')) {
        localStorage.setItem('favoriteQuotes', localStorage.getItem('favoriteQuotes') + "," + id);
    } else {
        localStorage.setItem('favoriteQuotes', id);
    }
    document.getElementById("quote_add_to_favorite").style.background = "url('./images/icons/star_checked.png')";
}

// Give column representing passed days and the current day this week a height
function loadBars() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", generate_url(barsType == BARS_TYPE_DAILY_FORECAST ? LINK_WEATHER_FORECAST : LINK_HOURLY_FORECAST), true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status == 200) {
                processBars(JSON.parse(xhr.responseText));
            }
        }
    };
    xhr.send();
}

function processBars(json) {
    let inputData = [];
    for (let i = 0; i < json.list.length; i++) {
        inputData[i] = json.list[i].main.temp;
    }
    let columnNames = [];
    switch (barsType) {
        case BARS_TYPE_HOURLY_FORECAST: {
            columnNames = getHoursList(json.list[0].dt_txt, json.list[6].dt_txt);
            break;
        }
        case BARS_TYPE_DAILY_FORECAST: {
            columnNames = getDayListStartingFrom(new Date().getDay() - 1);
            break;
        }
    }
    setRightBars(columnNames, inputData, 15, 60);
}

function processTemp(json) {
    document.getElementById("temperature").textContent = parseInt(json.main.temp) + "\u2103";
    document.getElementById("temperature_image").src = "./images/weather_icons/" + json.weather[0].icon + ".png";
}

function getDayListStartingFrom(startFromIndex) {
    let startIndex = startFromIndex % dayNames.length;
    return dayNames.slice(startIndex).concat(dayNames.slice(0, startIndex));
}

function getHoursList(startTime, endTime) {
    let result = [];
    result[0] = getHourMinutes(startTime);
    result[1] = "";
    result[2] = "";
    result[3] = "-";
    result[4] = "";
    result[5] = getHourMinutes(endTime);
    result[6] = "";
    result[7] = "";
    return result;
}

function getHourMinutes(time) {
    let date = new Date(time);
    let hour = date.getHours() % 24,
        minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    return hour + ":" + minute;
}

function map(x, in_min, in_max, out_min, out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function setRightBars(names, columnValues, minPercent, maxPercent) {
    let min = columnValues[0],
        max = columnValues[0];
    columnValues.forEach(function (temp) {
        if (temp > max) max = temp;
        if (temp < min) min = temp;
    });
    let percent;
    for (let i = 1; i <= 7; i++) {
        percent = map(columnValues[i - 1], min, max, minPercent, maxPercent);
        $("#x" + i).css({
            'height': +percent + '%'
        });
        document.getElementById("x" + i).textContent = parseInt(columnValues[i - 1]);
        document.getElementById("dl" + i).textContent = names[i - 1];
    }
}

$(function () {

    var date, dayName, day, month, year;
    var range = 270,
        sectionsDayName = 7,
        sectionsDay = 31,
        sectionsMonth = 12,
        charactersDayName = 3,
        charactersDay = 2,
        charactersMonth = 3,
        dayColor = '#FF2D55',
        monthColor = '#007AFF',
        dayNameColor = '#4CD964';


    // Rotate the selected ring the correct amount and illuminate the correct characters of the ring text
    function rotateRing(input, sections, characters, ring, text, color) {
        let sectionWidth = range / sections;
        let initialRotation = 135 - (sectionWidth / 2);
        let rotateAmount = initialRotation - sectionWidth * (input - 1);
        let start = (characters * (input - 1)) + (input - 1) + 1;

        $(ring).css({
            '-webkit-transform': 'rotate(' + rotateAmount + 'deg)',
            '-moz-transform': 'rotate(' + rotateAmount + 'deg)',
            '-ms-transform': 'rotate(' + rotateAmount + 'deg)',
            'transform': 'rotate(' + rotateAmount + 'deg)'
        });

        for (i = start; i < start + characters; i++) {
            $(text).children('.char' + i).css({
                'color': color
            });
        }
    }

    // Get a new date object every second and update the rotation of the clock handles
    function clockRotation() {
        setInterval(function () {
            var date = new Date();
            var seconds = date.getSeconds();
            var minutes = date.getMinutes();
            var hours = date.getHours();
            var secondsRotation = seconds * 6;
            var minutesRotation = minutes * 6;
            var hoursRotation = hours * 30 + (minutes / 2);
            $("#seconds").css({
                '-webkit-transform': 'rotate(' + secondsRotation + 'deg)',
                '-moz-transform': 'rotate(' + secondsRotation + 'deg)',
                '-ms-transform': 'rotate(' + secondsRotation + 'deg)',
                'transform': 'rotate(' + secondsRotation + 'deg)'
            });
            $("#minutes").css({
                '-webkit-transform': 'rotate(' + minutesRotation + 'deg)',
                '-moz-transform': 'rotate(' + minutesRotation + 'deg)',
                '-ms-transform': 'rotate(' + minutesRotation + 'deg)',
                'transform': 'rotate(' + minutesRotation + 'deg)'
            });
            $("#hours").css({
                '-webkit-transform': 'rotate(' + hoursRotation + 'deg)',
                '-moz-transform': 'rotate(' + hoursRotation + 'deg)',
                '-ms-transform': 'rotate(' + hoursRotation + 'deg)',
                'transform': 'rotate(' + hoursRotation + 'deg)'
            });
        }, 1000);
    }

    function init() {
        $(".center-preview").lettering();
        $(".day-name-preview").lettering();
        $(".day-name-text").lettering();
        $(".day-preview").lettering();
        $(".day-text").lettering();
        $(".month-preview").lettering();
        $(".month-text").lettering();
        $('.day-preview').fadeTo(10, 1);
        $('.month-preview').fadeTo(10, 1);
        $('.day-name-preview').fadeTo(10, 1);
        $('.center-preview').fadeTo(10, 1);

        if (typeof window.DOMParser != "undefined") {
            parseXml = function (xmlStr) {
                return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
            };
        } else if (typeof window.ActiveXObject != "undefined" &&
            new window.ActiveXObject("Microsoft.XMLDOM")) {
            parseXml = function (xmlStr) {
                xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = "false";
                xmlDoc.loadXML(xmlStr);
                return xmlDoc;
            };
        } else {
            // throw new Error("No XML parser found");
        }

        // Get date variables
        date = new Date();
        dayName = date.getDay(); // Day of week (1-7)
        day = date.getDate(); // Get current date (1-31)
        month = date.getMonth() + 1; // Current month (1-12)
        if (dayName == 0) {
            dayName = 7;
        }
        // Fade in/out second dial and rotate. Also fade in and animate side elements.
        setTimeout(function () {
            $('.day-preview').fadeTo(500, 0);
            $('.day-text').fadeTo(500, 1, function () {
                rotateRing(day, sectionsDay, charactersDay, '#r3', '.day-text', dayColor);
            });
        }, 500);

        // Fade in/out second dial and rotate. Also fade in and animate side elements.
        setTimeout(function () {
            $('.month-preview').fadeTo(500, 0);
            $('.fa-cloud').fadeTo(500, 1);
            $('.temperature').fadeTo(500, 1);
            $('.bars').fadeTo(500, 1);
            $('.month-text').fadeTo(500, 1, function () {
                rotateRing(month, sectionsMonth, charactersMonth, '#r2', '.month-text', monthColor);
            });
        }, 1000);

        // Fade in/out first dial and rotate
        setTimeout(function () {
            $('.day-name-preview').fadeTo(500, 0);
            $('.day-name-text').fadeTo(500, 1, function () {
                rotateRing(dayName, sectionsDayName, charactersDayName, '#r1', '.day-name-text', dayNameColor);
            });
        }, 1500);

        // Fade in/out center dial
        setTimeout(function () {
            $('.center-preview').fadeTo(500, 0);
            $('.head').fadeTo(500, 0);
            $('.torso').fadeTo(500, 0);
            $(".hand-container").fadeTo(500, 1, function () {
                //console.log("Clock faded in");
            });
        }, 2000);

        // Begin clock rotation now it is visible
        clockRotation();

        // setTimeout(loadHoro, 10);
        setTimeout(loadQuote, 20);
        setTimeout(checkTemp, 0.1 * 1000);
        setTimeout(loadBars, 0.6 * 1000);

        document.getElementById("steps").onclick = switchBars;
        document.getElementById("horo").onclick = switchHoro;
        document.getElementById("quote_text").onclick = loadQuote;
        document.getElementById("quote_add_to_favorite").onclick = addQuoteToFavorites;

        setInterval(checkTemp, 60 * 1000);
        setInterval(loadBars, 180 * 1000);
        // setInterval(loadHoro, 180 * 1000);
        setInterval(loadQuote, 240 * 1000);
    }

    init();
});